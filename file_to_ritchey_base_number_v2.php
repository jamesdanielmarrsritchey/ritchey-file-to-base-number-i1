<?php
#Name:File To Ritchey Base Number v2
#Description:Convert a file to a number using the Ritchey Base Number encoding scheme. Returns the number as a string on success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values.
#Arguments:'source' (required) is a path to the file to convert. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):source:file:required,display_errors:bool:optional
#Content:
if (function_exists('file_to_ritchey_base_number_v2') === FALSE){
function file_to_ritchey_base_number_v2($source, $display_errors = NULL){
	$errors = array();
	$progress = '';
	##Arguments
	if (@is_file($source) === FALSE){
		$errors[] = "source";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	}
	if ($display_errors === TRUE OR $display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task [Import file data to string. Convert string to Base64. Break base64 string into an array of single characters. Represent each letter as an incrementing number between 10 and 74 (eg: A = 10, B = 11, C = 12). Convert array to string. NOTE: Base64 can encode be done in chunks of 3 bytes and still achieve the same result as processing a file all at once, but this implementation does not process files 3 bytes at a time. There is no point since it is returning the number, not saving it to another file.]
	if (@empty($errors) === TRUE){
		###Import file to string
		$string = @file_get_contents($source);
		###Convert to base64
		$string = @base64_encode($string);
		###Convert string to array of single characters
		$string = @str_split($string, 1);
		###Convert each character in the array to a number between 10 and 74.
		foreach ($string as &$character){
			if ($character === "A"){
				$character = '10';	
			} else if ($character === "B"){
				$character = '11';
			} else if ($character === "C"){
				$character = '12';
			} else if ($character === "D"){
				$character = '13';
			} else if ($character === "E"){
				$character = '14';
			} else if ($character === "F"){
				$character = '15';
			} else if ($character === "G"){
				$character = '16';
			} else if ($character === "H"){
				$character = '17';
			} else if ($character === "I"){
				$character = '18';	
			} else if ($character === "J"){
				$character = '19';	
			} else if ($character === "K"){
				$character = '20';	
			} else if ($character === "L"){
				$character = '21';	
			} else if ($character === "M"){
				$character = '22';	
			} else if ($character === "N"){
				$character = '23';	
			} else if ($character === "O"){
				$character = '24';	
			} else if ($character === "P"){
				$character = '25';	
			} else if ($character === "Q"){
				$character = '26';	
			} else if ($character === "R"){
				$character = '27';	
			} else if ($character === "S"){
				$character = '28';	
			} else if ($character === "T"){
				$character = '29';	
			} else if ($character === "U"){
				$character = '30';	
			} else if ($character === "V"){
				$character = '31';	
			} else if ($character === "W"){
				$character = '32';	
			} else if ($character === "X"){
				$character = '33';	
			} else if ($character === "Y"){
				$character = '34';	
			} else if ($character === "Z"){
				$character = '35';	
			} else if ($character === "a"){
				$character = '36';	
			} else if ($character === "b"){
				$character = '37';
			} else if ($character === "c"){
				$character = '38';
			} else if ($character === "d"){
				$character = '39';
			} else if ($character === "e"){
				$character = '40';
			} else if ($character === "f"){
				$character = '41';
			} else if ($character === "g"){
				$character = '42';
			} else if ($character === "h"){
				$character = '43';
			} else if ($character === "i"){
				$character = '44';	
			} else if ($character === "j"){
				$character = '45';	
			} else if ($character === "k"){
				$character = '46';	
			} else if ($character === "l"){
				$character = '47';	
			} else if ($character === "m"){
				$character = '48';	
			} else if ($character === "n"){
				$character = '49';	
			} else if ($character === "o"){
				$character = '50';	
			} else if ($character === "p"){
				$character = '51';	
			} else if ($character === "q"){
				$character = '52';	
			} else if ($character === "r"){
				$character = '53';	
			} else if ($character === "s"){
				$character = '54';	
			} else if ($character === "t"){
				$character = '55';	
			} else if ($character === "u"){
				$character = '56';	
			} else if ($character === "v"){
				$character = '57';	
			} else if ($character === "w"){
				$character = '58';	
			} else if ($character === "x"){
				$character = '59';	
			} else if ($character === "y"){
				$character = '60';	
			} else if ($character === "z"){
				$character = '61';	
			} else if ($character === "0"){
				$character = '62';	
			} else if ($character === "1"){
				$character = '63';	
			} else if ($character === "2"){
				$character = '64';	
			} else if ($character === "3"){
				$character = '65';	
			} else if ($character === "4"){
				$character = '66';	
			} else if ($character === "5"){
				$character = '67';	
			} else if ($character === "6"){
				$character = '68';	
			} else if ($character === "7"){
				$character = '69';	
			} else if ($character === "8"){
				$character = '70';	
			} else if ($character === "9"){
				$character = '71';	
			} else if ($character === "+"){
				$character = '72';	
			} else if ($character === "/"){
				$character = '73';	
			} else if ($character === "="){
				$character = '74';	
			} else {
				$errors[] = "task - Convert characters to numbers";
				goto result;
			}
		}
		###Convert array back to string.
		$string = @implode($string);
	}
	result:
	##Display Errors
	if ($display_errors === TRUE and @empty($errors === FALSE)){
		$message = @implode(", ", $errors);
		if (function_exists('file_to_ritchey_base_number_v2_format_error') === FALSE){
			function file_to_ritchey_base_number_v2_format_error($errno, $errstr){
				echo $errstr;
			}
		}
		set_error_handler("file_to_ritchey_base_number_v2_format_error");
		trigger_error($message, E_USER_ERROR);
	}
	##Return
	if (@empty($errors) === TRUE){
		return $string;
	} else {
		return FALSE;
	}
}
}
?>